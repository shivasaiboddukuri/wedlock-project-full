package com.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmailService;
import com.dao.UserDao;
import com.model.User;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class UserController {
    @Autowired
    UserDao userdao;
    
    @Autowired
    EmailService emailService;
    
    @GetMapping("getUsers")
    public List<User> getUsers() {
        return userdao.getUsers();
    }
    
    @GetMapping("getUserById/{userId}")
    public User getUserById(@PathVariable("userId") int userId){
        return userdao.getUserById(userId);
    }
    
    @GetMapping("findByEmail/{emailId}")
    public User findByEmail(@PathVariable("emailId") String emailId){
        return userdao.findByEmail(emailId);
    }
    
    @GetMapping("login/{emailId},{password}")
    public User login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
        return userdao.login(emailId, password);
    }
    
    @PostMapping("registerUser")
    public User registerUser(@RequestBody User user){
        System.out.println(user);
        return userdao.registerUser(user);
    }

    @PostMapping("registerUserotp")
    public String registerUserotp(@RequestBody User user, HttpSession session) {
        String otp = emailService.emailVerify(user.getEmailId(), user);
        session.setAttribute("otp", otp);
        session.setAttribute("Userdata", user);
        session.setAttribute("emailId", user.getEmailId());
        return "Registered Successfully!!!";
    }

    @PutMapping("emailVerify/{otp}")
    public String emailVerification(HttpSession session, @PathVariable("otp") String otp){
        if(session.getAttribute("otp").equals(otp)){
            userdao.registerUser((User)session.getAttribute("Userdata"));
            emailService.registerEmailSender(session.getAttribute("emailId").toString(), (User)session.getAttribute("Userdata"));
            return "Registered Successfully";
        } else {
            return "OTP Invalid";
        }
    }
    
    @GetMapping("findByName/{userName}")
    public User findByName(@PathVariable("userName") String userName){
        return userdao.findByName(userName);
    }
    
    @GetMapping("findByGender/{gender}")
    public List<User> findByGender(@PathVariable("gender") String gender) {
        return userdao.findByGender(gender);
    }
    
    @GetMapping("findByLocation/{location}")
    public List<User> findByLocation(@PathVariable("location") String location){
        return userdao.findByLocation(location);
    }
    
    @GetMapping("findBymotherTongue/{motherTongue}")
    public List<User> findBymotherTongue(@PathVariable("motherTongue") String motherTongue){
        return userdao.findBymotherTongue(motherTongue);
    }
    
    @GetMapping("findByJob/{job}")
    public List<User> findByJob(@PathVariable("job") String job){
        return userdao.findByJob(job);
    }
    
    @GetMapping("findByEducation/{education}")
    public List<User> findByEducation(@PathVariable("education") String education){
        return userdao.findByEducation(education);
    }
    
    @GetMapping("findMatches")
    public List<User> findMatches(){
        return userdao.findMatches();
    }
    
    @GetMapping("findMale")
    public List<User> findMale(){
        return userdao.findMale();
    }
    
    @GetMapping("findFemale")
    public List<User> findFemale(){
        return userdao.findFemale();
    }
    
    @PutMapping("updateUser")
    public String updateUser(@RequestBody User user) {
        userdao.updateUser(user);
        return "User Updated Successfully!!!";
    }
    
    @PutMapping("passwordReset/{emailId},{password}")
    public User userUpdate(@PathVariable("emailId") String emailId, @PathVariable() String password){
        return userdao.userUpdate(emailId, password);
    }

    // Send OTP for various purposes (e.g., email verification, password reset)
    @PostMapping("sendOtp")
    public ResponseEntity<Map<String, String>> sendOtp(@RequestBody String emailId, HttpSession session) {
        Map<String, String> response = new HashMap<>();
        try {
            String otp = emailService.sendOtpToEmail(emailId);
            session.setAttribute("otp", otp);
            session.setAttribute("emailId", emailId);
            response.put("message", "OTP sent successfully!");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.put("message", "Error sending OTP: " + e.getMessage());
            return ResponseEntity.status(500).body(response);
        }
    }

    // Validate the OTP
    @PutMapping("validateOtp/{emailId}/{otp}")
    public ResponseEntity<Map<String, String>> validateOtp(@PathVariable("emailId") String emailId, @PathVariable("otp") int otp, HttpSession session) {
        Map<String, String> response = new HashMap<>();
        try {
            String sessionOtp = (String) session.getAttribute("otp");
            String sessionEmailId = (String) session.getAttribute("emailId");
            
            if (sessionEmailId != null && sessionEmailId.equals(emailId) && sessionOtp != null && sessionOtp.equals(String.valueOf(otp))) {
                session.removeAttribute("otp");
                session.removeAttribute("emailId");
                response.put("message", "OTP Verified");
                return ResponseEntity.ok(response);
            } else {
                response.put("message", "OTP Verification Failed");
                return ResponseEntity.status(401).body(response);
            }
        } catch (Exception e) {
            response.put("message", "Exception occurred: " + e.getMessage());
            return ResponseEntity.status(500).body(response);
        }
    }

    // Reset Password after OTP validation
    @PutMapping("resetPassword")
    public ResponseEntity<Map<String, String>> resetPassword(@RequestBody Map<String, String> payload, HttpSession session) {
        String emailId = payload.get("emailId");
        String otp = payload.get("otp");
        String newPassword = payload.get("newPassword");

        Map<String, String> response = new HashMap<>();
        try {
            String sessionOtp = (String) session.getAttribute("otp");
            String sessionEmailId = (String) session.getAttribute("emailId");
            
            if (sessionEmailId != null && sessionEmailId.equals(emailId) && sessionOtp != null && sessionOtp.equals(otp)) {
                User user = userdao.findByEmail(emailId);
                if (user != null) {
                    user.setPassword(newPassword); // Ensure the password is hashed before saving
                    userdao.updateUser(user);
                    session.removeAttribute("otp");
                    session.removeAttribute("emailId");
                    response.put("message", "Password reset successfully!");
                    return ResponseEntity.ok(response);
                } else {
                    response.put("message", "User not found");
                    return ResponseEntity.status(404).body(response);
                }
            } else {
                response.put("message", "OTP verification failed");
                return ResponseEntity.status(401).body(response);
            }
        } catch (Exception e) {
            response.put("message", "Exception occurred: " + e.getMessage());
            return ResponseEntity.status(500).body(response);
        }
    }
}



//package com.Controller;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpSession;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.dao.EmailService;
//import com.dao.UserDao;
//import com.model.User;
//
//@RestController
//@CrossOrigin(origins="http://localhost:4200")
//public class UserController {
//	@Autowired
//	UserDao userdao;
//	
//	@Autowired
//	EmailService emailSerivce;
//	
//	
//	@GetMapping("getUsers")
//	public List<User> getUsers() {
//		return userdao.getUsers();
//	}
//
//	
//	@GetMapping("getUserById/{userId}")
//	public User getUserById(@PathVariable("userId") int userId){
//		return userdao.getUserById(userId);
//	}
//	
//	@GetMapping("findByEmail/{emailId}")
//	public User findByEmail(@PathVariable("emailId") String emailId){
//		return userdao.findByEmail(emailId);
//	}
//	
//	@GetMapping("login/{emailId},{password}")
//	public User login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
//
//	  return userdao.login(emailId, password);
//	}
//	
//	@PostMapping("registerUser")
//	public User registerUser(@RequestBody User user){
//		System.out.println(user);
//		return userdao.registerUser(user);
////		return "Registered Successfully";
//	}
//
//	@PostMapping("registerUserotp")
//	public String registerUserotp(@RequestBody User user,HttpSession session) {
//		String otp = emailSerivce.emailVerify(user.getEmailId(), user);
//		
//		session.setAttribute("otp", otp);
//		session.setAttribute("Userdata", user);
//		session.setAttribute("emailId", user.getEmailId());
//		return "Registered Successfully!!!";
//	}
//
//	@PutMapping("emailVerify/{otp}")
//	public String emailVerification(HttpSession session,@PathVariable("otp") String otp){
//		
//		if(session.getAttribute("otp").equals(otp)){
//			userdao.registerUser((User)session.getAttribute("Userdata"));
//			emailSerivce.registerEmailSender(session.getAttribute("emailId").toString(),(User)session.getAttribute("Userdata"));
//		    return "Registered Successfully";
//		}
//		else{
//			return "OTP Invalid";
//		}
//	}
//	
//	@GetMapping("findByName/{userName}")
//	public User findByName(@PathVariable("userName") String userName){
//		return userdao.findByName(userName);
//	}
//	
//	@GetMapping("findByGender/{gender}")
//    public List<User> findByGender(@PathVariable("gender") String gender) {
//        return userdao.findByGender(gender);
//    }
//	
//	@GetMapping("findByLocation/{location}")
//	public List<User> findByLocation(@PathVariable("location") String location){
//		return userdao.findByLocation(location);
//	}
//	
//	@GetMapping("findBymotherTongue/{motherTongue}")
//	public List<User> findBymotherTongue(@PathVariable("motherTongue") String motherTongue){
//		return userdao.findBymotherTongue(motherTongue);
//	}
//	
//	@GetMapping("findByJob/{job}")
//	public List<User> findByJob(@PathVariable("job") String job){
//		return userdao.findByJob(job);
//	}
//	
//	@GetMapping("findByEducation/{education}")
//	public List<User> findByEducation(@PathVariable("education") String education){
//		return userdao.findByEducation(education);
//	}
//	
//	@GetMapping("findMatches")
//	public List<User> findMatches(){
//		return userdao.findMatches();
//	}
//	
//	@GetMapping("findMale")
//	public List<User> findMale(){
//		return userdao.findMale();
//	}
//	
//	@GetMapping("findFemale")
//	public List<User> findFemale(){
//		return userdao.findFemale();
//	}
//	
//	
//	@PutMapping("updateUser")
//	public String updateUser(@RequestBody User user) {
//		userdao.updateUser(user);
//		return "user Updated Successfully!!!";
//	}
//	
//	@PutMapping("passwordReset/{emailId},{password}")
//	public User userUpdate(@PathVariable("emailId") String emailId,@PathVariable() String password){
//		return userdao.userUpdate(emailId, password);
//	}
//	
//	
//	@PostMapping("getEmailOtp")
//	public ResponseEntity<Map<String, String>> EmailOtp(@RequestBody String emailId) {
//		Map <String, String> response = new HashMap<>();
//		if(userdao.generateOTP(emailId)) {
//			response.put("message", "OTP is Sent");
//			return ResponseEntity.ok(response);
//		}
//		else {
//			return null;
//		}
//		
//	}
//	
//	@PutMapping("validateEmailOtp/{emailId}/{otp}")
//	public ResponseEntity<Map<String, String>> validateEmailOtp(@PathVariable("emailId") String emailId, @PathVariable("otp") int otp) {
//		Map <String, String> response = new HashMap<>();
//		if(userdao.validateEmailOtp(emailId, otp)) {
//			response.put("message", "OTP Verified");
//			return ResponseEntity.ok(response);
//		}
//		else {
//			return null;
//		}
//	}
//
//
//}
